# Tri par insertion

Il s'agit du mode de tri le plus simple.

Sa complexité est de l'ordre O(n²), ce qui le rend inéficace sur les grands tableaux
