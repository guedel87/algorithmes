<?php declare(strict_types=1);

function intersection(array $left, array $right): array
{
    $inter = [];
    foreach ($left as $leftItem) {
        if (in_array($leftItem, $right)) {
            $inter[] = $leftItem;
        }
    }
    return $inter;
}

// must display [1, 4]
    var_dump(intersection(
            [1, 2, 4, 5],
            [1, 4, 6])
    );
