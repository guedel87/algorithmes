<?php declare(strict_types=1);

    function intersection(iterable $left, iterable $right): array
    {
        $inter = [];
        sort($left);
        sort($right);

        $itLeft = new ArrayIterator($left);
        $itRight = new ArrayIterator($right);

        while ($itLeft->valid() && $itRight->valid()) {
            if ($itLeft->current() < $itRight->current()) {
                $itLeft->next();
            } elseif ($itLeft->current() > $itRight->current()) {
                $itRight->next();
            } else {
                $inter[] = $itLeft->current();
                $itLeft->next();
                $itRight->next();
            }
        }
        return $inter;
    }

    // must display [1, 4]
    var_dump(intersection(
            [1, 2, 4, 5],
            [1, 4, 6])
    );
