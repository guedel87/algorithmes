# Intersection de 2 listes

Le principe est simple. Il s'agit de restituer la liste des éléments
commnuns dans les 2 listes fournies.

L'algorithme de base utilise un comportement naïf et reste gourmand en temps 
d'exécution et donc est réservé pour les petites listes.

Pour les listes de grande taille un autre algorithme est proposé et dispose d'une complexité 
d'ordre O^n.


## Algorithme simple

```
algorithme intersection

fonction externe existe<T>(element: T, liste: Liste<T>): booléen;

fonction intersection<T>(gauche, droite: Liste<T>): Liste<T>
    var element: T
    var retour: Liste<T>
    
    pour chaque element dans gauche
        si droite.contient(element) alors
            retour.ajoute(element)
        fin si
    fin pour

    retourne retour
fin fonction

```

On voit bien que pour chaque élément de la liste de gauche, il faut également parcourir
l'ensemble de la liste de droite pour vérifier l'éxistence d'un élément.

Une autre approche consiste à partir avec deux listes triées.

```
algorithme intersection2

fonction intersection<T>(gauche, droite: Liste<T>): Liste<T>
    var itGauche, itDroite : Iterateur<Liste<T> >
    var resultat = instancie Liste<T>()
    
    tri(gauche)
    tri(droite)
    
    itGauche = instancie Iterateur<Liste<T> >(gauche)
    itDroite = intancie Iterateur<Liste<T> >(droite)
    
    tantque itGauche.valide ET itDroite.valide
        si itGauche.actuel < itDroite.actuel alors
            itGauche.suivant
        sinonsi itGauche.actuel > idDroite.actuel alors
            itDroite.suivant
        sinon
            itGauche.suivant
            itDroite.suivant
            resultat.ajoute(itGauche.actuel)            
        fin si
    fin tantque
    
    retourne resultat
fin fonction

```