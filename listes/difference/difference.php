<?php declare(strict_types=1);

/**
 * Returns left items not in right list
 * @param array $left
 * @param array $right
 * @return array
 */
function difference(array $left, array $right): array
{
    $diff = [];
    foreach ($left as $leftItem) {
        if (! in_array($leftItem, $right)) {
            $diff[] = $leftItem;
        }
    }
    return $diff;
}

// must display [2, 5]
var_dump(difference(
    [1, 2, 4, 5],
    [1, 4, 6])
);