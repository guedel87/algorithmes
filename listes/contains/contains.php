<?php declare(strict_types=1);

function contains(array $array, mixed $item): bool
{
    foreach ($array as $value) {
        if ($value === $item) {
            return true;
        }
    }
    return false;
}

// must display yes
echo contains([1, 5, 7], 5) ? 'yes': 'no', PHP_EOL;

// must display no
echo contains([1, 5, 7], 4) ? 'yes': 'no', PHP_EOL;