# Les listes 

Cette section traite de différents algorithmes sur les listes.

Le terme liste comprend toutes les structures qui sont itérables.

Ainsi les tableaux et les collections en font partie.

Toutefois, pour une compréhension commune, nous allons définir un modèle
de classe qui définit la liste :

```
classe Liste<T>

    // Ajoute un élément à la liste
    procédure ajoute(élément: T);
    
    // Indique l'existance de l'élément dans la liste
    fonction contient(élément: T): booléen;

fin classe
```

Avec ça nous aurons besoin d'un itérateur :
```
interface Iterateur<T>

    // retourne l'objet courant
    fonction actuel(): T;
    
    // passe à l'objet suivant
    procédure suivant();
    
    // indique que la position courante est valide
    fonction valide(): booléen;

    // réinitialise la position
    procédure retour();
fin interface
```