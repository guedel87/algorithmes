# Algorithmes

Recensement d'algorithmes

Vous trouverez ici la [descriptions de la syntaxe utilisée](syntaxe.md)

## Les tris
  - [Tri par insertion](tri/tri-selection/readme.md)
  - [Tri Shell](tri/tri-shell/readme.md)
  - [Tri rapide](tri/tri-rapide/readme.md)

## Les arbres
  - [Création d'un arbre d'après une liste de parent](arbre/creation-avec-parents/readme.md)
  - [Création d'une d'après une liste de dépendences](arbre/creation-avec-dependances/readme.md)

## Les chaînes de caractères
  - [Concaténation avec séparateur](chaines/concatenation-separateur/readme.md)
  - [Insertion d'une chaine](chaines/insertion/readme.md)
  - [Distance de Levenshtein](chaines/distance-levenshtein/readme.md)
