<?php
// Règles de dépendances
$deps = array(
    'a' => array('b', 'c'),
    'd' => array('e'),
    'e' => array('f', 'g'),
    'h' => array(),
);

/**
 * Création d'un arbre d'après les règles de dépendances
 * @param type $rules
 */
function create_tree($rules) {
    $top = array();
    // on récupère tous ceux qui ne sont pas dans une règle
    foreach ($rules as $key => $deps) {
        if (! in_rule($key, $rules)) {
            $top[$key] = get_tree_of($key, $rules);
        }
    }
    return $top;
}

/**
 * Indique si l'élément est utilisé dans une règle
 * @param type $item
 * @param type $rules
 * @return boolean
 */
function in_rule($item, $rules)
{
    foreach ($rules as $deps) {
        foreach ($deps as $token) {
            if ($token == $item) {
                return true;
            }
        }
    }
    return false;
}

/**
 * Retourne l'arbre qui correspond à l'élément
 * @param mixed $item
 * @param array $rules
 * @return array
 */
function get_tree_of($item, $rules)
{
    $ret = array();
    if (isset($rules[$item])) {
        $rule = $rules[$item];
        foreach ($rule as $item) {
            $ret[$item] = get_tree_of($item, $rules);
        }
    }
    return $ret;
}

/**
 * Sortie de l'arbre
 * @param type $tree
 */
function dump_tree($tree)
{
    echo '<ul>';
    foreach ($tree as $key => $subtree) {
        echo '<li>', $key;
        dump_tree($subtree);
        echo '</li>';
    }
    echo '</ul>';
}

dump_tree(create_tree($deps));
