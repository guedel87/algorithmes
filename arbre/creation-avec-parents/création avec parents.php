<?php
// Liste des parents
  $parent = array(
      'a' => null,
      'b' => 'a',
      'c' => 'a',
      'd' => null,
      'e' => 'd',
      'f' => 'e',
      'g' => 'e',
      'h' => null,
  );

  /**
   * Création d'un arbre d'après les règles de dépendances
   * @param array $rules
   */
  function create_tree($rules)
  {
    return get_tree_of(null, $rules);
  }

  /**
   * Retourne l'arbre qui correspond à l'élément
   * @param string $item
   * @param array $rules
   * @return type
   */
  function get_tree_of($item, $rules)
  {
    $ret = array();
    foreach ($rules as $child => $parent) {
      if ($parent === $item) {
        $ret[$child] = get_tree_of($child, $rules);
      }
    }
    return $ret;
  }

  /**
   * Sortie de l'arbre
   * @param type $tree
   */
  function dump_tree($tree)
  {
    echo '<ul>';
    foreach ($tree as $key => $subtree) {
      echo '<li>', $key;
      dump_tree($subtree);
      echo '</li>';
    }
    echo '</ul>';
  }

  dump_tree(create_tree($parent));
