# Création d'un arbre d'après une liste de parent

Cet algorithme permet de construire un arbre d'après une liste de parent.

Il peut être adapté pour parcourir un arbre sans avoir à le construire
