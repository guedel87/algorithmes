# Insertion de chaine dans une chaine

Il s'agit d'un basique. Il arrive régulièrement que nous soyons 
obligé d'insérer une chaine dans une autre.

Cela tient en une instruction. L'algorithme qui suit est plus détaillé

```
ALRORITHME insertion-chaine

  /* réalise l'extraction d'une sous-chaine à partir de "début" 
  et d'une longueur de "longueur"
  L'index du première caractère est en position 1
  */
  FONCTION EXTERNE extrait(source : CHAINE, début, longueur: ENTIER)

  FONCTION insertion(source, insère: CHAINE; position: ENTIER): CHAINE
    VAR
      g, d: CHAINE

      g <- extrait(source, 1, position-1)
      d <- extrait(source, position)

      RETOURNE concatène(g, insère, d)
  FIN FONCTION

  /* Variante sans l'utilisation de la fonction d'extraction
  */
  FONCTION insertion2(source, insère: CHAINE; position: ENTIER): CHAINE
    VAR 
      résultat: CHAINE = ""
      index : ENTIER

    # on prend le début
    POUR index de 1 à position - 1
      résultat <- concatène(résultat, source[index])
    FIN POUR
    # on ajoute la partie à insérer
    résultat <- concatène(résultat, insère)

    # on ajoute le reste
    POUR index de position à longueur(source)
      résultat <- concatène(résultat, source[index])
    FIN POUR 
    RETOURNE résultat
  FIN FONCTION

  # Exemple
  ECRIRE insertion("c'est mon chien", "petit ", 10)

  # Résultat attendu : c'est mon petit chien

```
