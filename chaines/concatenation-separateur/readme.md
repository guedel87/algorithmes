# Concaténation de chaine avec séparateur

L'algorithme fonctionnne également pour une sortie console.

- [L'algorithme textuel](./concatenation-separateur.algo)
- [Le diagramme d'activité](./concatenation-separateur.puml)

## Exemple en PHP
```PHP
<?php
$first = true;
$items = ['a', 'b', 'c'];
foreach ($items as $item) {
    if ($first) {
        $first = false;
        $concat = $item;
    } else {
        $concat .= ',' . $item;
    }
}

echo $concat;
```

## Exemple en C#
```C#
bool first = true;
string[] items = { 'a', 'b', 'c' };
string concat = '';

foreach(var item in items) {
    if (first) {
        first = false;
        concat += item;
    } else {
        concat += ',' + item;
    }
}

Console.WriteLine(concat);
```
