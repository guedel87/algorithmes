<?php

function LevenshteinDistance(string $a, string $b) : int 
{
  $n = strlen($a);
  $m = strlen($b);
  $d = [];

  for ($i = 0; $i <= $n; $i++) {
    $d[$i][0] = $i;

  }
  for ($j = 0; $j <= $m; $j++) {
    $d[0][$j] = $j;
  }
  for ($j = 1; $j <= $m; $j++) {
    for ($i = 1; $i <= $n; $i++) {
      $cout = ($a[$i-1] == $b[$j-1]) ? 0: 1;
      $d[$i][$j] = min($d[$i-1][$j] + 1, $d[$i][$j-1] + 1, $d[$i-1][$j-1] + $cout );
    }
  }
  return $d[$n][$m];
}

echo LevenshteinDistance("kitten", "sitting");
echo PHP_EOL;
echo LevenshteinDistance("golang", "pascal");
// Doit afficher 3 puis 6
