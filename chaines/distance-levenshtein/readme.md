# Distance de Levenshtein

La distance de Levenshtein mesure la distance entre deux séquences. Elle indique le nombre de remplacement à réaliser pour qu'une chaîne de caractères soit égale à une autre.

## Les algorithmes

### L'implémentation classique
- [L'algorithme textuel](distance-levenshtein.algo)
- [Le diagramme d'activité](./distance-levenshtein.puml)

## Exemples
- [en PHP](./distance-levenshtein.php)
