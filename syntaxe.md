## Syntaxe utilisée

### Types de données de base
- ENTIER: réprésente un entier relatif
- NOMBRE: représente un nombre décimal à virgule flottante
- CARACTERE: réprésente un simple caractère
- CHAINE, CHAINE(n), TEXTE: représente une chaine de caractère.
- TABLEAU [X à Y] DE type: représente un tableau
- REFERENCE DE type: réprésente une référence sur un type de données
- ENUMERATION {a; b; c}: une énumération de symboles
- QUELCONQUE: réprésente une donnée de n'importe quel type

### Déclarations
Les types de données 

### Type de données évolués
#### Structure
```
  TYPE a: STRUCTURE
    champ1 : type1
    champ2, champ3: type2
  FIN STRUCTURE
```

#### Classe
```
  TYPE c: CLASSE
    VAR attribut: ENTIER

    FONCTION a(): ENTIER
      RETOURNE ceci.attribut
    FIN FONCTION

    PROCEDURE b()
    FIN PROCEDURE
  FIN CLASSE
```

### Exemple
```
ALGORITHME exemple
  # Les constantes sont introduites par "CONST" ou "CONSTANTE"
  CONST N = 10
  # Les type sont introduits par "TYPE" ou "DOMAINE"
  TYPE nom_type : définition type
  # Les variables sont introduites par "VAR" ou "VARIABLE"
  VARIABLE
    # On peut en mettre sur plusieurs lignes avec une indentation
    i : ENTIER
    s : TEXTE
    n : NOMBRE
    a : TABLEAU [1 à N] de NOMBRE
  
  PROCEDURE ma_procédure(arg1: NOMBRE; arg2, arg3: QUELCONQUE)
    SI a < b ALORS 
      # instructions
    SINON
      # instructions
    FIN SI

    TANT QUE a < 10 FAIRE
      # instructions
    FIN TANT QUE

    POUR a de 1 à 10 FAIRE
      # instructions
    FIN POUR

    REPETE
      # instructions
    JUSQUA a > 10
  FIN PROCEDURE

  FONCTION factorielle(n: NOMBRE) : NOMBRE
    SI n <= 1 ALORS
      RETOURNE 1
    FIN SI
    RETOURNE n * factorielle(n - 1)
  FIN FONCTION

  ma_procédure 10, "a", 56.7
FIN ALGORITHME

```
